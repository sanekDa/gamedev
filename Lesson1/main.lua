require("fly")
require("perlin")

function love.load()
    love.window.setTitle("Random walks")
    background = love.graphics.newImage("resourses/background.png")
    width = background:getWidth()
    height = background:getHeight()
    love.window.setMode(width, height)
    love.graphics.setBackgroundColor(3 / 255, 182 / 255, 252/255)

    fly = Fly:create("resourses/mosquito_small.png", 200, 300, 1)
    noisex = Noise:create(1, 1, 256)
    noisey = Noise:create(1, 1, 256)
    tx = 0
    ty = -1
    -- herd = HerdFlies:create("resourses/mosquito_small.png", 0, width, 0, height, 100)
end

function love.draw()
    love.graphics.draw(background, 0, 0)

    fly:draw()
    -- herd:draw()

    love.graphics.print("FPS: " .. tostring(love.timer.getFPS()), 10, 10)
end

function love.update(dt)
    -- herd:update()
    tx = tx + 0.01
    ty = ty + 0.015
    x = noisex:calc(tx)
    y = noisex:calc(ty)
    fly.x = remap(x, 0, 1, 50, width - 50)
    fly.y = remap(y, 0, 1, 50, height - 50)
end
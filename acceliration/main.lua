require("vector")
require("mover")

function love.load()
    love.graphics.setBackgroundColor(150/255, 150/255, 150/255)
    mover = Mover:random()
    -- mover.acceleration.x = 0.01
    -- mover.acceleration.x = -0.01
end

function love.draw()
    mover:draw()
    love.graphics.print("mag = " .. tostring(mover.velocity:mag()), 10, 10)
end

function love.update(dt)
    -- acceleration = Vector:random(-2, 2, -2, 2) * 0.5
    x, y = love.mouse.getPosition()
    mouse = Vector:create(x, y)
    dir = mouse - mover.location
    acceleration = dir:norm() * 0.75
    mover.acceleration = acceleration
    mover:update()
    mover:check_boundaries()
    mover.velocity = mover.velocity:limit(10)
end
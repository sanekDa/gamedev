Mover = {}
Mover.__index = Mover

function Mover:create(location, velocity)
    local mover = {}
    setmetatable(mover, Mover)
    mover.location = location
    mover.velocity = velocity
    mover.acceleration = Vector:create(0, 0)
    return mover
end

function Mover:random()
    local location = Vector:create()
    location.x = love.math.random(0, love.graphics.getWidth())
    location.y = love.math.random(0, love.graphics.getHeight())
    local velocity = Vector:create()
    velocity.x = love.math.random(-2, 2)
    velocity.y = love.math.random(-2, 2)
    return Mover:create(location, velocity)
end

function Mover:draw()
    love.graphics.circle("fill", self.location.x, self.location.y, 20)
end

function Mover:update()
    self.velocity = self.velocity + self.acceleration
    self.location = self.location + self.velocity
end

function Mover:check_boundaries()
    local width = love.graphics.getWidth()
    local height = love.graphics.getHeight()
    if self.location.x > width then
        self.location.x = 0
    elseif self.location.x < 0 then
        self.location.x = width 
    end  
    if self.location.y > height then
        self.location.y = 0
    elseif self.location.y < 0 then
        self.location.y = height 
    end  
end
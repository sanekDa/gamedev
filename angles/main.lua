require("vector")
require("mover")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    local location = Vector:create(width / 4 * 3, height / 2)
    local velocity = Vector:create(0,0)
    mover = Mover:random()
    mover.aAcceleration = 0.01
end

function love.update(dt)
    mover:check_boundaries()
    mover:update()
end

function love.draw()
    mover:draw()
end

function love.keypressed(key)

end
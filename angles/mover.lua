Mover = {}
Mover.__index = Mover

function Mover:create(location, velocity, mass)
    local mover = {}
    setmetatable(mover, Mover)
    mover.location = location
    mover.velocity = velocity
    mover.acceleration = Vector:create(0, 0)
    mover.mass = mass or 1
    mover.size = 20 * mover.mass
    mover.aAcceleration = 0
    mover.aVelocity = 0
    return mover
end

function Mover:random()
    local location = Vector:create()
    location.x = love.math.random(0, love.graphics.getWidth())
    location.y = love.math.random(0, love.graphics.getHeight())
    local velocity = Vector:create()
    velocity.x = love.math.random(-2, 2)
    velocity.y = love.math.random(-2, 2)
    return Mover:create(location, velocity)
end

function Mover:attract(mover)
    local forceV = self.location - mover.location
    distance = forceV:mag()
    if distance then
        forceV = forceV:norm()
        if distance < 5 then
            distance = 5
        end

        if distance > 25 then
            distance = 25
        end
        strength = (G * self.mass * mover.mass) / (distance * distance)
        forceV:mul(strength)
        return forceV
    end
    return nil
end

function Mover:draw()
    love.graphics.push()
    love.graphics.translate(self.location.x, self.location.y)
    love.graphics.rotate(self.aVelocity)
    love.graphics.rectangle("fill", -self.size / 2, -self.size / 2, self.size, self.size)
    love.graphics.pop()
end

function Mover:update()
    self.velocity:add(self.acceleration)
    self.location:add(self.velocity)
    self.aVelocity = self.aVelocity + self.aAcceleration
    self.acceleration:mul(0)
end

function Mover:check_boundaries()
    if self.location.x > width - self.size then
        self.location.x = width - self.size
        self.velocity.x = -1 * self.velocity.x
    elseif self.location.x < self.size then
        self.location.x = self.size
        self.velocity.x = -1 * self.velocity.x
    end  
    if self.location.y > height - self.size then
        self.location.y = height - self.size
        self.velocity.y = -1 * self.velocity.y
    elseif self.location.y < self.size then
        self.location.y = self.size
        self.velocity.y = -1 * self.velocity.y
    end  
end

function Mover:applyForce(force)
    self.acceleration:add(force * self.mass)
end
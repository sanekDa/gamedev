Vehicle = {}
Vehicle.__index = Vehicle

function Vehicle:create(x, y)
    local vehicle = {}
    setmetatable(vehicle, Vehicle)
    vehicle.position = Vector:create(x,y)
    vehicle.velocity = Vector:create(0,0)
    vehicle.acceleration = Vector:create(0,0)

    vehicle.r = 5
    vehicle.verticles = {0, -vehicle.r*2, -vehicle.r,vehicle.r*2,vehicle.r,vehicle.r*2}

    vehicle.maxSpeed = 4
    vehicle.maxForce = 0.1

    vehicle.wtheta = 0

    return vehicle
end

function Vehicle:update()
    self.velocity:add(self.acceleration)
    self.velocity:limit(self.maxSpeed)
    self.position:add(self.velocity)
    self.acceleration:mul(0)
end

function Vehicle:applyForce(force)
    self.acceleration:add(force)
end

function Vehicle:seek(target)
    local desired = target - self.position
    local mag = desired:mag()
    desired:norm()
    if mag < 100 then
        local m = math.map(mag, 0, 100, 0, self.maxSpeed)
        desired:mul(m)
    else
        desired:mul(self.maxSpeed)
    end
    local steer = desired - self.velocity
    steer:limit(self.maxForce)
    self:applyForce(steer)
end

function Vehicle:flee(target)
    local desired = target - self.position
    local mag = - desired:mag()
    desired:norm()
    print(mag)
    if mag > -100 then
        local m = math.map(mag, 0, 100, 0, self.maxSpeed)
        desired:mul(m)
    else
        desired:mul(0)
    end
    local steer = desired - self.velocity
    steer:limit(self.maxForce)
    self:applyForce(steer)
end

function Vehicle:wander()
    local rwander = 25
    local dwander = 80
    self.wtheta = self.wtheta + love.math.random(-30, 30) / 100
    local pos = self.velocity:copy()
    pos:norm()
    pos:mul(dwander)
    pos:add(self.position)

    local h = self.velocity:heading()

    local offset = Vector:create(rwander * math.cos(self.wtheta + h), rwander * math.sin(self.wtheta + h))

    local target = pos + offset
    self:seek(target)

    love.graphics.circle("line", pos.x, pos.y, rwander)
    love.graphics.circle("fill", target.x, target.y, 4)
end

function Vehicle:borders()
    if self.position.x < -self.r then
        self.position.x = width + self.r
    end
    if self.position.y < -self.r then
        self.position.y = height + self.r
    end
    if self.position.x > width + self.r then
        self.position.x = -self.r
    end
    if self.position.y > height + self.r then
        self.position.y = -self.r
    end
end

function Vehicle:boundaries()
    local desired = nil
    if self.position.x < d then
        desired = Vector:create(self.maxSpeed, self.velocity.y)
    elseif self.position.x > width - d then
        desired = Vector:create(-self.maxSpeed, self.velocity.y)
    end

    if self.position.y < d then
        desired = Vector:create(self.velocity.x, self.maxSpeed)
    elseif self.position.y > height - d then
        desired = Vector:create(self.velocity.x, -self.maxSpeed)
    end

    if desired then
        desired:norm()
        desired:mul(self.maxSpeed)
        local steer = desired - self.velocity
        steer:limit(self.maxForce)
        self:applyForce(steer)
    end
end

function Vehicle:draw()
    local theta = self.velocity:heading() + math.pi/2
    love.graphics.push()
    love.graphics.translate(self.position.x,self.position.y)
    love.graphics.rotate(theta)
    love.graphics.polygon("fill",self.verticles)
    love.graphics.pop()
end

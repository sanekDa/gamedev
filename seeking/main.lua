require("Vector")
require("Vehicle")


function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    d = 100

    vehicle = Vehicle:create(width/2,height/2)
    vehicle.velocity.y = 1
    vehicle.velocity.x = 2
end

function love:update()
    local x,y = love.mouse.getPosition()
    -- vehicle:seek(Vector:create(x,y))
    -- vehicle:flee(Vector:create(x,y))
    vehicle:boundaries()
    vehicle:update()
end

function love.draw()
    -- vehicle:wander()
    vehicle:draw()
    love.graphics.rectangle("line",d,d,width - 2 * d,height - 2 * d)
end
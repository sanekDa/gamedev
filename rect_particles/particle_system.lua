ParticleSystem = {}
ParticleSystem.__index = ParticleSystem

function ParticleSystem:create(x,y, size)
    local system = {}
    setmetatable(system, ParticleSystem)
    system.origin = Vector:create(x, y)
    system.n = 4
    system.particles = {}
    system.index = 0
    system.size = size
    system.isclicked = false
    return system
end

function ParticleSystem:draw()
    r, g, b, a = love.graphics.getColor()
    for k, v in pairs(self.particles) do
        v:draw()
    end
    love.graphics.setColor(r, g, b, a)
end

function ParticleSystem:update()
    -- print(#self.particles)
    
    if self.isclicked == false and #self.particles < self.n then
        -- print(#self.particles, self.n)
        for type = 1, 4, 1 do
            self.particles[type] = Particle:create(self.origin.x, self.origin.y, type - 1, self.size)
        end
    end
    for k, v in pairs(self.particles) do
        if v then
            if v:isDead() then
                table.remove(self.particles, k)
            end
            if self.isclicked == true then
                v.start = true
            end
            v:update()
        end
    end
end

function ParticleSystem:applyForce(force)
    for k, v in pairs(self.particles) do
        v:applyForce(force)
    end
end

function ParticleSystem:applyRepeller(repeller)
    for k, v in pairs(self.particles) do
        local force = repeller:repel(v)
        v:applyForce(force)
    end
end

function ParticleSystem:clicked(mousex, mousey)
    local d = math.dist(mousex, mousey, self.origin.x, self.origin.y)
    if d < self.size / 2 then
        self.isclicked = true
    end
end
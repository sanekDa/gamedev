require("vector")
require("particle")
require("repeller")
require("particle_system")

function math.dist(x1, y1, x2, y2)
  return ((x2 - x1)^2 + (y2 - y1)^2)^0.5
end

function love.load()

  width = love.graphics.getWidth()
  height = love.graphics.getHeight()
  textures = {}
  textures.heart = love.graphics.newImage("heart.png")
  textures.fusion = love.graphics.newImage("texture.png")
  particles = {}
  
  local size = 50
  quantity = 108
  
  local x = size
  local y = size

  for i = 1, quantity do
    particles[i] = ParticleSystem:create(x, y, size)
    x = x + size * 1.25
    if x >= width then
      x = size
      y = y + size * 1.25
    end
  end
end

function love.draw()
  for i = 1, quantity do
    particles[i]:draw()
  end
end

function love.update(dt)
  -- system:update()
  for i = 1, quantity do
    particles[i]:update()
  end
end


function love.keypressed(key) 
end



function love.mousepressed(x, y, button, istouch)
  if button == 1 then
    for i = 1, quantity do
      particles[i]:clicked(x, y)
    end
      -- system:clicked(x, y)
  end
end
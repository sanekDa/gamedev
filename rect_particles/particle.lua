Particle = {}
Particle.__index = Particle

function Particle: create(x, y, type, size)
    local particle  = {}
    setmetatable(particle, Particle)
    particle.location = Vector:create(x,y)
    particle.velocity = Vector:create(math.random(-200,200) / 20,
                                        math.random(-200,200) / 20)
    particle.lifespan = 100
    particle.acceleration = Vector:create(0, 0.05)
    particle.decay = math.random(3, 10) / 10
    particle.texture = textures.fusion
    particle.p1 = Vector:create(0,0)
    particle.p2 = Vector:create(0,0)
    particle.type = type
    if type == 0 then
        particle.p1.x = particle.location.x - size / 2
        particle.p1.y = particle.location.y - size / 2
        particle.p2.x = particle.location.x + size / 2
        particle.p2.y = particle.location.y - size / 2
    elseif type == 1 then
        particle.p1.x = particle.location.x + size / 2
        particle.p1.y = particle.location.y - size / 2
        particle.p2.x = particle.location.x + size / 2
        particle.p2.y = particle.location.y + size / 2
    elseif type == 2 then
        particle.p1.x = particle.location.x + size / 2
        particle.p1.y = particle.location.y + size / 2
        particle.p2.x = particle.location.x - size / 2
        particle.p2.y = particle.location.y + size / 2
    elseif type == 3 then
        particle.p1.x = particle.location.x - size / 2
        particle.p1.y = particle.location.y + size / 2
        particle.p2.x = particle.location.x - size / 2
        particle.p2.y = particle.location.y - size / 2
    end
    particle.size = size
    
    particle.start = false
    return particle
end

function Particle:update()
    if self.start then
        self.acceleration = Vector:create(math.random(),math.random())
        self.velocity:add(self.acceleration)
        self.p1:add(self.velocity)
        self.p2:add(self.velocity)
        self.acceleration:mul(0)
    end
    -- self.lifespan = self.lifespan - self.decay
end

function Particle:applyForce(force)
    self.acceleration:add(force)
end
function Particle: draw()
    r,g,b,a = love.graphics.getColor()
    if self.start then
        love.graphics.setColor(0.88, 0.34, 0.13, self.lifespan / 100)
    else 
        love.graphics.setColor(0.88, 0.34, 0.13, self.lifespan)
    end
    love.graphics.line(self.p1.x, self.p1.y, self.p2.x, self.p2.y)
    love.graphics.setColor(r,g,b,a)
end

function Particle: isDead()
    return self.lifespan < 0
  end
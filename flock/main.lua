require("vector")
require("boid")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    boids = {}
    for i = 0, 100 do
        boids[i] = Boid:create(width / 2, height / 2)
    end

    isSep = false
    isAlign = false
    isCohesion = false
end

function love:update()
    local x, y = love.mouse.getPosition()
    print(x,y)
    local target = Vector:create(x, y)
    for i = 0, #boids do
        boids[i]:update(boids, target)
    end

end

function love.draw()
    for i = 0, #boids do
        boids[i]:draw()
    end
    
    r, g, b, a = love.graphics.getColor()
    if isSep then
        love.graphics.setColor(1, 0, 0)
    else
        love.graphics.setColor(0, 0, 1)
    end
    love.graphics.circle("fill", 10, 10, 5)
    love.graphics.setColor(r, g, b, a)

    r, g, b, a = love.graphics.getColor()
    if isAlign then
        love.graphics.setColor(1, 0, 0)
    else
        love.graphics.setColor(0, 0, 1)
    end
    love.graphics.circle("fill", 40, 10, 5)
    love.graphics.setColor(r, g, b, a)

    r, g, b, a = love.graphics.getColor()
    if isCohesion then
        love.graphics.setColor(1, 0, 0)
    else
        love.graphics.setColor(0, 0, 1)
    end
    love.graphics.circle("fill", 70, 10, 5)
    love.graphics.setColor(r, g, b, a)
end

function love.keypressed(key)
    if key == "1" then
        isSep = not isSep
    elseif key == "2" then
        isAlign = not isAlign
    elseif key == "3" then
        isCohesion = not isCohesion
    end
end
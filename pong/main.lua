require("vector")
require("ball")
require("platform")

function love.load()
    love.graphics.setBackgroundColor(0/255, 0/255, 0/255)
    state = "start"
    font_name = "resources/AtariClassic-Regular.ttf"
    font_small = love.graphics.newFont(font_name, 10)
    font_big = love.graphics.newFont(font_name, 24)
    p1_score = 0
    p2_score = 0
    ball = Ball:create(Vector:create(395,285), Vector:create(0,0))
    p1_platform = Platform:create(Vector:create(60,280), Vector:create(0,0), 40)
    p2_platform = Platform:create(Vector:create(720,280), Vector:create(0,0), 40)
    soundWin = love.audio.newSource("resources/victory.wav", "static")
    soundLoose = love.audio.newSource("resources/gameover.wav", "static")
    timeToPass = nil
end

function love.draw()
    if state == "start" then
        love.graphics.setFont(font_big)
        love.graphics.print("Pong Game", 300, 200)
        love.graphics.print("Press any key to start!", 120, 300)
    elseif state == "game" or state == "magnet1" then
        love.graphics.line(400, 0, 400, 600)
        love.graphics.rectangle("line", 30, 60, 740, 500)
        love.graphics.setFont(font_big)
        love.graphics.print(tostring(p1_score), 350, 20)
        love.graphics.print(tostring(p2_score), 430, 20)
        love.graphics.setFont(font_small)
        love.graphics.print("FPS: " .. tostring(love.timer.getFPS()), 700, 580)
        p1_platform:draw()
        p2_platform:draw()
        ball:draw()
    elseif state == "win" then
        love.graphics.setFont(font_big)
        love.graphics.print("You WIN!!!", 300, 200)
        love.graphics.print("Press any key to restart!", 120, 300)
    elseif state == "gameover" then
        love.graphics.setFont(font_big)
        love.graphics.print("You loose", 300, 200)
        love.graphics.print("Press any key to restart!", 120, 300)
        
    end
end

function love.update(dt)
    if state == "game" then
        if p1_score == 9 then
            state = "win"
        elseif p2_score == 9 then
            state = "gameover"
        end
        ball:check_left_platform(p1_platform)
        ball:check_right_platform(p2_platform)
        p1_platform:update()
        
        ball:update()
        local ret = ball:check_boundaries(30, 770, 60, 560)
        if (ret == 1) then
            p2_score = p2_score + 1
            ball.location.x = p1_platform.location.x + p1_platform.width
            ball.location.y = p1_platform.location.y + p1_platform.width - ball.size/2
            state = "magnet1"
            ball.velocity = Vector:create(0,0)
            soundLoose:play()
        elseif (ret == 2) then
            p1_score = p1_score + 1
            ball.location.x = p2_platform.location.x 
            ball.location.y = p2_platform.location.y + p2_platform.width - ball.size/2
            soundWin:play()
        end

        p2_platform:aim(ball)
        p2_platform:update()
        p1_platform:check_boundaries(60, 560)
        p2_platform:check_boundaries(60, 560)

    elseif state == "magnet1" then
        ball.location.x = p1_platform.location.x + p1_platform.width
        ball.location.y = p1_platform.location.y + p1_platform.width - ball.size/2
        p1_platform:update()
        ball:update()
        p1_platform:check_boundaries(60, 560)
        p2_platform:check_boundaries(60, 560)
    end
end

function love.keypressed(key)
    print(state, ball.velocity)
    if state == "start" then
        state = "game"
    elseif state == "gameover" or state == "win" then
        love.load()
    elseif state == "game" or state == "magnet1" then
        if key == "space" and state == "magnet1" then
            state = "game"
            ball.velocity = Vector:random(3, 4, -3, 3)
            ball.velocity.y = ball.velocity.y + p1_platform.velocity.y
            ball.velocity.x = 2 + (p1_score + p2_score) / 3
            while ball.velocity.y == 0 do
                ball.velocity.y = love.math.random(-3, 3)
            end
        elseif key == "space" and ball.velocity.x == 0 and ball.velocity.y == 0 then
            ball.velocity = Vector:random(-3, 3, -3, 3)
            ball.velocity.x = 2 + (p1_score + p2_score) / 3
            while ball.velocity.y == 0 do
                ball.velocity.y = love.math.random(-3, 3)
            end
        end
        if key == "up" then
            p1_platform.velocity.y = -4
        elseif key == "down" then
            p1_platform.velocity.y = 4
        end
    end
end
Ball = {}
Ball.__index = Ball

function Ball:create(location, velocity)
    local ball = {}
    setmetatable(ball, Ball)
    ball.velocity = velocity
    ball.location = location
    ball.size = 10
    ball.sound = love.audio.newSource("resources/pong.wav", "static")
    return ball
end

function Ball:draw()
    love.graphics.rectangle("fill", self.location.x, self.location.y, 10, 10)
end

function Ball:update()
    self.location = self.location + self.velocity
end

function Ball:check_boundaries(left, right, top, bottom)
    if self.location.x <= left then
        self.sound:play()
        self.velocity.x = self.velocity.x * -1
        return 1
    end

    if self.location.x >= right - self.size then
        self.sound:play()
        self.velocity.x = self.velocity.x * -1
        return 2
    end

    if self.location.y <= top or self.location.y >= bottom - self.size then
        self.sound:play()
        self.velocity.y = self.velocity.y * -1
        return 3
    end
    return 4
end

function Ball:check_left_platform(platform)
    if self.location.x <= platform.location.x + platform.width and
    self.location.x >= platform.location.x and
    self.location.y >= platform.location.y and 
    self.location.y <= platform.location.y + platform.height then
        self.location.x = platform.location.x + platform.width
        self.velocity.x = - self.velocity.x
        self.sound:play()
    end
end

function Ball:check_right_platform(platform)
    if self.location.x >= platform.location.x and
    self.location.x <= platform.location.x + platform.width and
    self.location.y >= platform.location.y and 
    self.location.y <= platform.location.y + platform.height then
        self.location.x = platform.location.x
        self.velocity.x = - self.velocity.x
        self.sound:play()
    end
end
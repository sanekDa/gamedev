Platform = {}
Platform.__index = Platform

function Platform:create(location, velocity, height)
    local platform = {}
    setmetatable(platform, Platform)
    platform.velocity = velocity
    platform.location = location
    platform.height = height
    platform.width = 20
    return platform
end

function Platform:draw()
    love.graphics.rectangle("fill", self.location.x, self.location.y, self.width, self.height)
end

function Platform:update()
    self.location = self.location + self.velocity
end

function Platform:check_boundaries(bottom, top)
    if self.location.y >= top - self.height then
        self.location.y = top - self.height
    end
    if self.location.y <= bottom then
        self.location.y = bottom
    end
end

function Platform:aim(ball)
    local dir = ball.location - p2_platform.location
    local acceleration = dir:norm() * 0.75
    self.velocity.y = self.velocity.y + acceleration.y
end


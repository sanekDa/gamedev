Ground = {}
Ground.__index = Ground

function Ground:create(x1, y1, x2, y2)
    local ground = {}
    setmetatable(ground, ground)
    ground.x1 = x1
    ground.y1 = y1
    ground.x2 = x2
    ground.y2 = y2 
    return ground
end

function Ground:draw()
    love.graphics.push()
    love.graphics.translate(self.location.x, self.location.y)
    love.graphics.rotate(self.angle)
    love.graphics.rectangle("fill", -self.width / 2, -self.height / 2, self.width, self.height)
    love.graphics.pop()
end
require("vector")
require("mover")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    local location = Vector:create(width / 2, height/ 4)
    local velocity = Vector:create(0, 0)
    mover = Mover:create(location,velocity, 40, 40)
    mover.angle = 60

    local location = Vector:create(width / 4 *2 , height / 3 * 2)
    local velocity = Vector:create(0, 0)
    ground  = Mover:create(location, velocity, width/2, 40)
    ground.angle = 15

    gravity = Vector:create(0, 0.01)
    isGravity = false
end

function love.update(dt)
    if isGravity then
        mover:applyForce(gravity)
    end
    mover:update()
    check_collisions()
    mover:check_boundaries()
    
end

function check_collisions()
    xg = ground.location.x
    yg = ground.location.y
    xm = mover.location.x
    ym = mover.location.y
    if xm + mover.width / 2 > xg - ground.width / 2
    and xm - mover.width / 2 < xg + ground.width / 2 
    and ym + mover.height / 2 > yg - ground.height / 2
    and ym - mover.height / 2 < yg + ground.height / 2 then
        isGravity = false
        mover.angle = ground.angle
        mover.velocity = Vector:create(0,0)
    end
end

function love.draw()
    mover:draw()
    ground:draw()
end

function love.keypressed(key)
    if (key == "g") then
        isGravity = not isGravity
    end
end
function love.load()
    print("load callback")
    love.window.setTitle("Callbacks")
    background = love.graphics.newImage("house.png")
    love.graphics.setBackgroundColor(3 / 255, 182 / 255, 252/255)
    fps = 0

end

function love.draw()
    love.graphics.draw(background, 0, 0)
    love.graphics.print(string.format("%.2f", fps), 10, 10)
end

function love.update(dt)
    fps = 1. / dt
end

function love.mousepressed(x, y, button, istouch)
    print("Mouse pressed: ", x, y, button, istouch)
end

function love.mousereleased(x, y, button, istouch)
    print("Mouse released: ", x, y, button, istouch)
end

function love.keypressed(key)
    print("Key pressed: ", key)
end

function love.keyreleased(key)
    print("Key released: ", key)
end

function love.focus(f)
    print("Focus: ", f)
end

function love.quit()
    print("Quit callback")
end
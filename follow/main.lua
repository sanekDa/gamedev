require("vector")
require("mover")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    mover = Mover:random()
end

function love.update(dt)
    x, y = love.mouse.getPosition()
    mouse = Vector:create(x, y)
    dir = mouse - mover.location
    mover.acceleration = dir:norm() * 0.4

    mover.velocity = mover.velocity:limit(10)
    mover:check_boundaries()
    mover:update()
end

function love.draw()
    mover:draw()
end

function love.keypressed(key)

end
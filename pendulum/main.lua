require("vector")
require("pendulum")

function math.dist(x1, y1, x2, y2)
    return ((x2 - x1)^2 + (y2 - y1)^2)^0.5
end

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    pendulum = Pendulum:create(Vector:create(width / 2, 200), 200)
    gravity = 0.4
end

function love.update(dt)
    pendulum:update()
    -- print(love.mouse.getPosition())
end

function love.draw()
    pendulum:draw()
end

function love.keypressed(key)
end

function love.mousepressed(x, y, button, istouch)
    if button == 1 then
        pendulum:clicked(x, y)
    end
end

function love.mousereleased(x, y, button, istouch)
    if button == 1 then
        pendulum:stopDragging()
    end
end
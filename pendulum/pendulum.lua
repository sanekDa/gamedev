Pendulum = {}
Pendulum.__index = Pendulum

function Pendulum:create(origin, length)
    local pendulum = {}
    setmetatable(pendulum, Pendulum)
    pendulum.origin = origin
    pendulum.length = length
    pendulum.position = Vector:create(0, 0)
    pendulum.angle = math.pi / 4
    pendulum.r = 20
    pendulum.aVelocity = 0
    pendulum.aAcceleration = 0
    pendulum.damping = 0.995
    isDrag = false
    return pendulum
end


function Pendulum:draw()
    love.graphics.setColor(255, 255, 255, 1)
    self.position.x = self.length * math.sin(self.angle) + self.origin.x
    self.position.y = self.length * math.cos(self.angle) + self.origin.y
    love.graphics.circle("line", self.origin.x, self.origin.y, 5)
    love.graphics.line(self.origin.x, self.origin.y, self.position.x, self.position.y)

    if self.isDrag then
        love.graphics.setColor(255, 0, 0, 1)
    else
        love.graphics.setColor(255, 255, 255, 1)
    end
    love.graphics.circle("fill", self.position.x, self.position.y, self.r)
end

function Pendulum:update()
    self:drag()
    self.aAcceleration = (-1 * gravity / self.length) * math.sin(self.angle)
    self.aVelocity = self.aVelocity + self.aAcceleration
    self.aVelocity = self.aVelocity * self.damping
    self.angle = self.angle + self.aVelocity
    local mouse_pos = Vector:create(love.mouse.getX(), love.mouse.getY())
end

function Pendulum:clicked(mousex, mousey)
    local d = math.dist(mousex, mousey, self.position.x, self.position.y)
    if d < self.r then
        self.isDrag = true
    end
end

function Pendulum:stopDragging()
    if self.isDrag then
        self.aVelocity = 0
        self.isDrag = false
    end
end

function Pendulum:drag(mousex, mousey)
    if self.isDrag then
        local x, y = love.mouse.getPosition()
        local diff = self.origin - Vector:create(x, y)
        self.angle = math.atan2(-1 * diff.y, diff.x) - math.pi / 2
    end
end
function loadTextures()
    env = {}
    env.tileset = love.graphics.newImage("assets/RogueEnvironment16x16.png")

    local quads = {
        {0, 5*16, 0*16}, -- floor v1
    }

    env.textures = {}
    for i = 1, #quads do
        local q = quads[i]
        env.textures[q[1]] = love.graphics.newQuad(q[2], q[3], 16, 16, env.tileset:getDimensions())
    end
end

function love.load()
    loadTextures()
end

function love.draw()
    
end

function love.update(dt)
    for i=1, #env.textures do
        texture = env.textures[i]
        love.graphics.draw(env.tileset, texture, 16 * i, 32)
    end
end

function love.keypressed(key)
    
end
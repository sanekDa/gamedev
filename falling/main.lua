require("vector")
require("mover")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    local location = Vector:create(width / 3, height / 3)
    local velocity = Vector:create(0, 0)
    rect1 = Mover:create(location, velocity, 50, 50, 45)

    local location = Vector:create(width - width / 3, height / 3)
    local velocity = Vector:create(0, 0)
    rect2 = Mover:create(location, velocity, 50, 50, -25)

    local location = Vector:create(width / 3, height - height / 3)
    local velocity = Vector:create(0, 0)
    ground1 = Mover:create(location, velocity, 200, 50, -25)

    local location = Vector:create(width - width / 3, height - height / 3)
    local velocity = Vector:create(0, 0)
    ground2 = Mover:create(location, velocity, 200, 50, 25)

    gravity = Vector:create(0, 0.1)
end

function love.update(dt)
    if isGravity then
        rect1:applyForce(gravity)
        rect2:applyForce(gravity)
    end
    rect1:checCollision(ground1)
    rect2:checCollision(ground2)

    rect1:update();
    rect2:update();
end

function love.draw()
    rect1:draw()
    rect2:draw()
    ground1:draw();
    ground2:draw();
end


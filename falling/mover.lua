Mover = {}
Mover.__index = Mover

function Mover:create(location, velocity, width, height, angle)
    local mover = {}
    setmetatable(mover, Mover)
    mover.location = location
    mover.velocity = velocity
    mover.aVelocity = 0
    mover.aAcceleration = 0
    mover.acceleration = Vector:create(0, 0)
    mover.width = width
    mover.height = height
    mover.angle = angle or 0
    mover.active = false
    mover.mod = 1
    return mover
end

function Mover:draw()
    love.graphics.push()
    love.graphics.translate(self.location.x, self.location.y)
    love.graphics.rotate(math.rad(self.angle))
    
    r, g, b, a = love.graphics.getColor()
    
    love.graphics.rectangle("fill", -self.width / 2, -self.height / 2, self.width, self.height)

    love.graphics.setColor(r, g, b, a)
    love.graphics.pop()
end

function Mover:applyForce(force)
    self.acceleration:add(force)
end

function Mover:checkBoundaries()
    if self.location.x < 0 then
        self.location.x = width
    end
    if self.location.x > width then
        self.location.x = 0
    end
    if self.location.y < 0 then
        self.location.y = height
    end
    if self.location.y > height then
        self.location.y = 0
    end
end

function Mover:checCollision(ground)
    xg = ground.location.x
    yg = ground.location.y
    xm = self.location.x + self.velocity.x
    ym = self.location.y + self.velocity.y + 10
    if xm + self.width / 2 > xg - ground.width / 2
    and xm - self.width / 2 < xg + ground.width / 2 
    and ym + self.height / 2 > yg - ground.height / 2
    and ym - self.height / 2 < yg + ground.height / 2 then
        isGravity = false
        self.mod = ground.angle - self.angle
        self.mod = self.mod / math.abs(self.mod)
        self.angle = self.angle + ground.angle / 8 
        self.velocity = Vector:create(-2 * self.mod, 1)
    else
        -- print(self.location)
        isGravity = true
        self.angle = self.angle + ground.angle / 16 
    end

end

function Mover:update()
    self.velocity = self.velocity + self.acceleration
    self.location = self.location + self.velocity
    self.aVelocity = self.aVelocity + self.aAcceleration
    self.acceleration:mul(0)
end


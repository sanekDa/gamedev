Fly = {}
Fly.__index = Fly

function Fly:create(image_path, position, speed)
    local fly = {}
    setmetatable(fly, Fly)
    fly.image_path = image_path
    fly.image = love.graphics.newImage(fly.image_path)
    fly.position = position
    fly.speed = speed 
    return fly

end

function Fly:draw()
    love.graphics.draw(self.image, self.position.x, self.position.y)

end

function Fly:update()
    self.position = self.position + self.speed
end

function Fly:checkBoundaries(minx, maxx, miny, maxy)
   
    if self.position.x <= minx or self.position.x > maxx then
        self.speed.x = self.speed.x * -1
    end
    if self.position.y <= miny or self.position.y > maxy then
        self.speed.y = self.speed.y * -1
    end
end
require("vector")

function love.load()
    width = 600
    height = 400
    love.window.setMode(width, height)
    love.graphics.setBackgroundColor(150/255, 150/255, 150/255)
    center = Vector:create(width / 2, height / 2)
end

function love.draw()
    x, y = love.mouse.getPosition()
    mouse = Vector:create(x, y)
    diff = mouse - center
    
    -- diff = diff * 0.5
    diff = diff:norm()
    diff = diff * 50
    mag = diff:mag()

    line_end = center + diff
    love.graphics.setColor(1, 0, 0)
    love.graphics.line(center.x, center.y, line_end.x, line_end.y)
    love.graphics.rectangle("fill", 0 ,0, mag, 10)
end

function love.update(dt)

end
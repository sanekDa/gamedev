Attractor = {}
Attractor.__index = Attractor

function Attractor:create(location, mass)
    local attractor = {}
    setmetatable(attractor, Attractor)
    attractor.location = location
    attractor.mass = mass
    attractor.size = mass * 0.3 + 30
    attractor.inner_size = attractor.size
    return attractor
end


function Attractor:attract(mover)
    local forceV = self.location - mover.location
    distance = forceV:mag()
    if distance then
        forceV = forceV:norm()
        if distance < 5 then
            distance = 5
        end

        if distance > 25 then
            distance = 25
        end
        strength = (G * self.mass * mover.mass) / (distance * distance)
        forceV:mul(strength)
        return forceV
    end
    return nil
end

function Attractor:draw()
    r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(100/255, 200/255, 200/255)
    love.graphics.circle("line", self.location.x, self.location.y, self.size)
    self.inner_size = self.inner_size - 0.5
    if self.inner_size <= 0 then
        self.inner_size = self.size
    end
    love.graphics.circle("fill", self.location.x, self.location.y, self.inner_size)
    love.graphics.setColor(r, g, b, a)
end
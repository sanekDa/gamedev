require("vector")
require("mover")
require("attractor")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    G = -4

    -- local location = Vector:create(100, 100)
    -- local velocity = Vector:create(0.1, 0.05)
    
    movers = {}
    n = 10
    for i = 1, n do
        local location = Vector:create(love.math.random(20, width), love.math.random(20, height))
        local velocity = Vector:create(0, 0)
        mover = Mover:create(location, velocity, 0.5)
        movers[i] = mover
    end
    -- mover = Mover:create(location, velocity, 2)

    -- local location = Vector:create(width/4, height/2)
    -- attractor = Attractor:create(location, 20)

    -- local location = Vector:create(width/4*3, height/2)
    -- attractor2 = Attractor:create(location, 40)
end

function love.update(dt)
    
    for i = 1, n do
        for j = 1, n do
            if not(i == j) then
                local f = movers[i]:attract(movers[j])
                if f then
                    movers[j]:applyForce(f)
                end
                movers[j]:check_boundaries()
                movers[j]:update()
            end
        end
    end
end

function love.draw()
    -- attractor:draw()
    -- attractor2:draw()
    for i = 1, n do
        movers[i]:draw()
    end
end

function love.keypressed(key)
    if (key == "g") then
        
    end
end
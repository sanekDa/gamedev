require("vector")
require("vehicle")
require("flow")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    debug = true

    vehicles = {}
    quantity = 10
    for i = 0, quantity do
        vehicles[i] = Vehicle:create(width / 2 + love.math.random(-20, 20), height / quantity * i + love.math.random(-50, 50))
        vehicles[i].velocity.x = 3
    end
    -- vehicle = Vehicle:create(width / 2,height / 2)
    -- vehicle.velocity.x = 3


    flow = FlowField:create(40)
    flow:init()
end

function love:update()
    for i = 0, quantity do
        vehicles[i]:borders()
        vehicles[i]:follow(flow)
        vehicles[i]:update()
    end
end

function love.draw()
    for i = 0, quantity do
        vehicles[i]:draw()
    end
    if debug then
        flow:draw()
    end
end
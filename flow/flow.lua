FlowField = {}
FlowField.__index = FlowField

function FlowField:create(resolution)
    local flow = {}
    setmetatable(flow, FlowField)
    flow.field = {}
    flow.resolution = resolution
    love.math.setRandomSeed(10000)
    return flow
end

function FlowField:init()
    local cols = width / self.resolution
    local rows = height / self.resolution
    for i = 0, cols do
        self.field[i] = {}
        for j = 0, rows do
            -- local center = Vector:create(cols / 2, rows / 2)
            -- local center = Vector:create(cols, rows / 2)
            -- local dir = center - Vector:create(i, j)
            -- local dir = Vector:create(love.math.random(0, 1), love.math.random(0, 1))
            -- dir:norm()
            -- self.field[i][j] = dir
            local theta = math.map(love.math.noise(i, j), 0, 1, 0, math.pi * 2)
            self.field[i][j] = Vector:create(math.cos(theta), math.sin(theta))
            
        end
    end
end

function FlowField:lookup(v)
    local col = math.floor(v.x / self.resolution)
    local row = math.floor(v.y / self.resolution)  
    col = math.constrain(col, 0, #self.field)
    row = math.constrain(row, 0, #self.field[col])
    return self.field[col][row]:copy()
end

function FlowField:draw()
    r, g, b, a = love.graphics.getColor()
    love.graphics.setColor(1, 1, 1, 0.3)
    for i = 0, #self.field do
        for j = 0, #self.field[i] do
            -- love.graphics.circle("fill", i * self.resolution, j * self.resolution, 4)
            drawVector(self.field[i][j], i * self.resolution, j * self.resolution, self.resolution - 2)
        end
    end
    love.graphics.setColor(r, g, b, a)
end

function drawVector(v, x, y, scale)
    love.graphics.push()
    love.graphics.translate(x, y)
    love.graphics.rotate(v:heading())
    local len = v:mag() * scale
    love.graphics.line(0, 0, len, 0)
    love.graphics.pop()
end
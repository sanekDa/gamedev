Background = {}
Background.__index = Background

function Background:create()
    local bc = {}
    setmetatable(bc, Background)
    bc.mainLayout = love.graphics.newImage("assets/sprites/background-day.png")
    bc.footer = love.graphics.newImage("assets/sprites/base.png")
    bc.startScreen = love.graphics.newImage("assets/sprites/message.png")
    bc.currentX = 0
    return bc
end

function Background:update(dt)
    self.currentX = self.currentX - dt * 100
    if self.currentX + self.footer:getWidth() < width then
        self.currentX = 0
    end
end

function Background:draw()
    love.graphics.draw(self.mainLayout, 0, 0)
    if state == "start" then
        love.graphics.draw(self.startScreen, (width - self.startScreen:getWidth()) / 2, height/7)
    end
end

function Background:drawFooter()
    love.graphics.draw(self.footer, self.currentX, height - self.footer:getHeight())
end
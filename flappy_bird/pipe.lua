Pipe = {}
Pipe.__index = Pipe

function Pipe:create(xlocation, debug)
    local pipe = {}
    setmetatable(pipe, Pipe)
    pipe.x = xlocation
    pipe.windowHeight = love.math.random(120, height/3)
    -- pipe.windowHeight = 50
    pipe.height_p1 = love.math.random(height/6*3, height/6*4)
    pipe.height_p2 = height - pipe.height_p1 - pipe.windowHeight
    pipe.skin = love.graphics.newImage("assets/sprites/pipe-green.png")
    pipe.width = pipe.skin:getWidth()
    pipe.box1 = Vector:create(pipe.x, height - pipe.height_p1)
    pipe.box2 = Vector:create(pipe.x - pipe.width, 0)
    pipe.windowBox = {
        Vector:create(pipe.box1.x, pipe.box1.y - pipe.windowHeight), 
        Vector:create(pipe.box1.x + pipe.width, pipe.box1.y - pipe.windowHeight), 
        Vector:create(pipe.box1.x + pipe.width, pipe.box1.y),
        Vector:create(pipe.box1.x, pipe.box1.y)
        }
    pipe.debug = debug or false
    pipe.passed = false
    return pipe
end

function Pipe:update(xdt)
    self.x = self.x - xdt
    if not self.passed and self.x + self.width < mover.location.x then
        counter.score = counter.score + 1
        self.passed = true
    end
    self.box1 = Vector:create(self.x, self.height_p1)
    self.box2 = Vector:create(self.x, 0)
    self.windowBox = {
        Vector:create(self.box1.x, self.box1.y - self.windowHeight), 
        Vector:create(self.box1.x + self.width, self.box1.y - self.windowHeight), 
        Vector:create(self.box1.x + self.width, self.box1.y),
        Vector:create(self.box1.x, self.box1.y)
        }
end

function Pipe:draw()
        love.graphics.draw(self.skin, self.x, self.height_p1)
        love.graphics.draw(self.skin, self.x, height -self.height_p2 - self.windowHeight*2, 0,1,-1)
        if self.debug then
            love.graphics.rectangle("line", self.windowBox[1].x, self.windowBox[1].y, self.width, self.windowHeight)
            love.graphics.rectangle("line", self.box1.x, self.box1.y, self.width, height - self.height_p1)
            love.graphics.rectangle("line", self.box2.x, 1, self.width, height - self.height_p2-self.windowHeight*2)
        end
end

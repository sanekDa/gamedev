require("vector")
require("mover")
require("pipe")
require("pipeSystem")
require("background")
require("counter")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    local location = Vector:create(width / 2 - 34 / 2, height/ 2 - 24 / 2)
    local velocity = Vector:create(0, 0)
    local debug = false
    mover = Mover:create(location, velocity, debug)
    gravity = Vector:create(0, 0.25)
    pipeSystem = PipeSystem:create(debug)
    bc = Background:create()
    counter = Counter:create()
    state = "start"
    speed = 3
end

function love.update(dt)
    if not(state == "loose" or state == "fall" or state=="pause") then
        bc:update(dt)
    end
    if state == "game" or state == "fall" then
        mover.angle = mover.angle + 0.05
        mover:update(dt, pipeSystem)

        if mover.angle < -math.pi / 4 then
            mover.angle = -math.pi / 4
        end
        if mover.angle > math.pi / 2 then
            mover.angle = math.pi / 2
        end
        if not (state == "loose") then
            pipeSystem:update()
        end
        mover:applyForce(gravity)
    elseif state == "start" then
        mover:update(dt, nil)
    end
end

function love.draw()
    bc:draw()
    pipeSystem:draw()
    bc:drawFooter()
    mover:draw()
    counter:draw()
end

function love.keypressed(key, scancode, isrepeat)
    isrepeat = false
    if state == "loose" then
        love.load()
    end
    if state == "start" then
        pipeSystem:init(3, speed)
        state = "game"
    end
    
    if state == "game" and (key == "up" or key == "space") then
        mover.velocity.y = -4
        mover.angle = mover.angle - 2
    end
    if (key == "return") then
        if state == "game" then
            state = "pause"
        elseif state == "pause" then
            state = "game"
        end
    end
end
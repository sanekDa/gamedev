Counter = {}
Counter.__index = Counter

function Counter:create()
    local counter = {}
    setmetatable(counter, Counter)
    counter.gg = love.graphics.newImage("assets/sprites/gameover.png")
    counter.numbers = {}
    counter.score = 0
    for i = 0, 9 do
        counter.numbers[i] = love.graphics.newImage("assets/sprites/"..tostring(i)..".png")
    end
    return counter
end

function Counter:drawScore()
    local ar = {}
    local string = tostring(self.score)
    local sumWidth = 0
    for i = 1, #string do
        ar[i] = self.numbers[tonumber(string:sub(i,i))]
        sumWidth = sumWidth + ar[i]:getWidth()
        sumWidth = sumWidth + 10
    end
    sumWidth = sumWidth - 10
    for i = 1, #ar do
        love.graphics.draw(ar[i], (width - sumWidth) / 2 + sumWidth / #ar * (i-1), (height - self.gg:getHeight()) / 5)
    end
end

function Counter:draw()
    if state == "loose" then
        love.graphics.draw(self.gg, (width - self.gg:getWidth()) / 2, (height - self.gg:getHeight()) / 3)
        self:drawScore()
    elseif state == "start" then

    else
        self:drawScore()
    end
end
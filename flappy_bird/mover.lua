Mover = {}
Mover.__index = Mover

function Mover:create(location, velocity, debug)
    local mover = {}
    setmetatable(mover, Mover)
    mover.location = location
    mover.velocity = velocity
    mover.acceleration = Vector:create(0, 0)
    mover.mass = 1
    mover.size = 30 * mover.mass
    mover.aAcceleration = 0
    mover.aVelocity = 0
    mover.angle = 0
    mover.active = false
    mover.frames = {
        love.graphics.newImage("assets/sprites/redbird-downflap.png"),
        love.graphics.newImage("assets/sprites/redbird-midflap.png"),
        love.graphics.newImage("assets/sprites/redbird-upflap.png")
    }
    mover.currentFrame = 0
    mover.time = 0
    mover.frameTime = 0.1
    mover.width = mover.frames[1]:getWidth()
    mover.height = mover.frames[1]:getHeight()
    mover.bbox = mover:createBbox()
    mover.debug = debug
    return mover
end

function Mover:createBbox()
    local bbox = {
        Vector:create(self.location.x, self.location.y), 
        Vector:create(self.location.x + self.width, self.location.y), 
        Vector:create(self.location.x + self.width, self.location.y + self.height), 
        Vector:create(self.location.x, self.location.y + self.height), 
    }
    return bbox
end

function Mover:update(dt, ps)
    if self.time > self.frameTime then
        self.currentFrame = self.currentFrame + 1
        self.time = 0
    end
    self.time = self.time + dt
    if ps then 
        self:checkCollisions(ps)
        self.velocity:add(self.acceleration)
        if self.location.y < -50 then 
            self.location.y = -50
        end
        self.location:add(self.velocity)
        self.aVelocity = self.aVelocity + self.aAcceleration
        self.acceleration:mul(0)
        self.bbox = self:createBbox()
    end
    if self.location.y + self.width >= height - bc.footer:getHeight() then
        self.location.y = height - bc.footer:getHeight() - self.width
    end
end


function Mover:draw()
    local frame = self.frames[1 + self.currentFrame % 3]
    love.graphics.push()
    love.graphics.translate(self.location.x, self.location.y)
    love.graphics.rotate(self.angle)
    love.graphics.draw(frame)
    r,g,b,a = love.graphics.getColor()
    if self.debug then
        for i = 1, 3 do
            love.graphics.line(self.bbox[i].x - self.location.x, self.bbox[i].y - self.location.y, self.bbox[i+1].x - self.location.x, self.bbox[i+1].y - self.location.y)
        end
        love.graphics.line(self.bbox[4].x - self.location.x, self.bbox[4].y - self.location.y, self.bbox[1].x - self.location.x, self.bbox[1].y - self.location.y)
    end
    love.graphics.setColor(r,g,b,a)
    love.graphics.pop()    
end

function Mover:checkCollisions(ps)
    local minDist = 999999
    local closestPipe = nil
    for i = 1, #ps.list do
        local pipe = ps.list[i]
        if pipe.x + pipe.width >= self.location.x then
            local dist = self.location:distTo(pipe.box1)
            if dist < minDist then
                minDist = dist
                closestPipe = pipe
            end
        end
    end
    if closestPipe then
        for i = 1, 4 do
            if (self.bbox[i].x >= closestPipe.windowBox[1].x) and (self.bbox[i].x <= closestPipe.windowBox[2].x) then
                if (self.bbox[i].y > closestPipe.windowBox[1].y) and (self.bbox[i].y < closestPipe.windowBox[3].y)
                then

                else 
                    if state == "game" then
                        state = "fall"
                        gravity.y = gravity.y * 3
                    end
                    mover.angle = mover.angle + 0.05
                end
            elseif (self.bbox[i].y > height - bc.footer:getHeight()) then
                state = "loose"
            end
        end
    end  
end

function Mover:applyForce(force)
    self.acceleration:add(force * self.mass)
end
PipeSystem = {}
PipeSystem.__index = PipeSystem

function PipeSystem:create(debug)
    local ps = {}
    setmetatable(ps, PipeSystem)
    ps.list = {}
    ps.speed = nil
    ps.quantity = nil
    ps.startPoint = width * 2
    ps.space = 200
    ps.debug = debug
    return ps
end

function PipeSystem:init(quantity, speed)
    self.speed = speed
    self.quantity = quantity
    for i = 1, quantity do
        local pipe = Pipe:create(self.startPoint, self.debug)
        self.list[i] = pipe
        self.startPoint = self.startPoint + self.space
    end
end

function PipeSystem:update()
    for i = 1, #self.list do 
        local pipe = self.list[i]
        if pipe.x < 0 - pipe.skin:getWidth() then 
            self.list[i] = Pipe:create(self.startPoint % width + width  + self.space, self.debug)
        end
        self.list[i]:update(self.speed)
    end
end

function PipeSystem:draw()
    for i = 1, #self.list do 
        local pipe = self.list[i]
        pipe:draw()
    end
end
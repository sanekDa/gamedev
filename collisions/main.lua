require("vector")
require("mover")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    speed = 1
    xc1 = 70
    xc2 = 700
    yc1 = 50
    yc2 = 50
    r1 = 35
    r2 = 45

    xr1 = 70
    xr2 = 600
    yr1 = 150
    yr2 = 150
    sr1 = 30
    sr2 = 40

    ax = 0
    ay = 250
    bx = 20
    by = 275
    
    dx = width
    dy = 250
    cx = width - 20
    cy = 275


    isMoving = false
    p1x = 60
    p1y = 340
    p2x = 0
    p2y = 360

    
    r3 = 30
    xc3 = width -r3
    yc3 = 350
end

function love.update(dt)
    xc1 = xc1 + speed
    xc2 = xc2 - speed
    xc1 = xc1 % 800
    xc2 = xc2 % 800

    xr1 = xr1 + speed
    xr2 = xr2 - speed
    xr1 = xr1 % 800
    xr2 = xr2 % 800

    ax = ax  + speed
    bx = bx + speed
    if not (ax % 800 == ax) and not (bx % 800 == bx) then
        ax = ax % 800
        bx = bx % 800
    end
    dx = dx - speed
    cx = cx - speed
    if not (dx % 800 == dx) and not (cx % 800 == cx) then
        dx = dx % 800
        cx = cx % 800
    end

    if isMoving then 
        p1x = p1x  + speed
        p2x = p2x + speed
        if not (p1x % 800 == p1x) and not (p2x % 800 == p2x) then
            p1x = p1x % 800
            p2x = p2x % 800
        end
        xc3 = xc3 - speed
        xc3 = xc3 % 800
    end
end

function circle_collisions()
    local dx = xc1 - xc2
    local dy = yc1 - yc2
    local dist = math.sqrt(dx*dx + dy*dy)
    return dist <= (r1 + r2)
end

function rect_collisions()
    local bottom = yr1 + sr1 > yr2
    local top = yr1 < yr2 + sr2
    local left = xr1 + sr1 > xr2
    local right = xr1 < xr2 + sr2
    return bottom and top and right and left
end

function line_collisions()
    local denom = (dy - cy) * (bx - ax) - (dx - cx) * (by - ay) 
    local ua = (dx - cx) * (ay - cy) - (dy - cy) * (ax - cx)
    local ua = ua / denom
    local ub = (bx - ax) * (ay - cy) - (by - ay) * (ax - cx)
    local ub = ub / denom
    return math.abs(ua)<=1 and math.abs(ub)<=1
end

function line_circle_collisions()
    local locp1x = p1x - xc3
    local locp1y = p1y - yc3
    local locp2x = p2x - xc3
    local locp2y = p2y - yc3

    local p2p1x = locp2x - locp1x
    local p2p1y = locp2y - locp1y

    local a = p2p1x * p2p1x + p2p1y * p2p1y
    local b = 2 * (p2p1x * locp1x + p2p1y * locp1y)
    local c = locp1x * locp1x + locp1y * locp1y - r3 * r3
    local d = b * b - 4 * a * c
    if d < 0 then
        return nil
    elseif d == 0 then
        local u = -b / (2 * a)
        local x = p1x + u * p2p1x
        local y = p1y + u * p2p1y
        if (u <= 1 and u >= 0) then
            return {{x,y}}
        else
            return nil
        end
    else
        local sqrt = math.sqrt(d)
        local u1 = (-b + sqrt) / (2 * a)
        local u2 = (-b - sqrt) / (2 * a)
        local x1 = p1x + u1 * p2p1x
        local y1 = p1y + u1 * p2p1y
        local x2 = p1x + u2 * p2p1x
        local y2 = p1y + u2 * p2p1y
        if (u1 <= 1 and u1 >= 0)  or (u2 <= 1 and u2 >= 0) then
            return {{x1, y1}, {x2, y2}}
        else
            return nil
        end
    end
end

function love.draw()
    love.graphics.setColor(1, 1, 1)
    if circle_collisions() then
        love.graphics.setColor(1,0,0)
    end
    love.graphics.circle("fill", xc1, yc1, r1)
    love.graphics.circle("fill", xc2, yc2, r2)
    love.graphics.setColor(1, 1, 1)
    if rect_collisions() then
        love.graphics.setColor(1,0,0)
    end
    love.graphics.rectangle("fill", xr1, yr1, sr1, sr1)
    love.graphics.rectangle("fill", xr2, yr2, sr2, sr2)
    love.graphics.setColor(1, 1, 1)
    if line_collisions() then
        love.graphics.setColor(1,0,0)
    end
    love.graphics.line(ax, ay, bx, by)
    love.graphics.line(cx, cy, dx, dy)
    love.graphics.setColor(1, 1, 1)

    local result = line_circle_collisions()
    if result then
        love.graphics.setColor(0,1,0)
        for  i =1, #result do 
            x = result[i][1]
            y = result[i][2]
            love.graphics.circle("fill", x, y, 4)
        end
        love.graphics.setColor(1, 1, 1)
    end
    love.graphics.line(p1x, p1y, p2x, p2y)
    love.graphics.circle("fill", xc3, yc3, r3)
    love.graphics.setLineWidth(1)
end

function love.keypressed(key)
    if (key == "g") then
        isMoving = not isMoving
    end
    if (key == "up") then
        speed = speed + 1
    end
    if (key == "down") then
        speed = speed - 1
    end
end
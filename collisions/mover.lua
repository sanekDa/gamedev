Mover = {}
Mover.__index = Mover

function Mover:create(location, velocity, width, height)
    local mover = {}
    setmetatable(mover, Mover)
    mover.location = location
    mover.velocity = velocity
    mover.acceleration = Vector:create(0, 0)
    mover.mass = mass or 1
    mover.width = width
    mover.height = height
    mover.aAcceleration = 0
    mover.aVelocity = 0
    mover.angle = 0
    return mover
end

function Mover:random()
    local location = Vector:create()
    location.x = love.math.random(0, love.graphics.getWidth())
    location.y = love.math.random(0, love.graphics.getHeight())
    local velocity = Vector:create()
    velocity.x = love.math.random(-2, 2)
    velocity.y = love.math.random(-2, 2)
    return Mover:create(location, velocity)
end


function Mover:draw()
    love.graphics.push()
    love.graphics.translate(self.location.x, self.location.y)
    love.graphics.rotate(self.angle)
    love.graphics.rectangle("fill", -self.width / 2, -self.height / 2, self.width, self.height)
    love.graphics.pop()
end

function Mover:update()
    self.velocity:add(self.acceleration)
    self.location:add(self.velocity)
    self.aVelocity = self.aVelocity + self.aAcceleration
    self.acceleration:mul(0)
end

function Mover:check_boundaries()
    if self.location.x > width - self.width then
        self.location.x = width - self.width
        self.velocity.x = -1 * self.velocity.x
    elseif self.location.x < self.width then
        self.location.x = self.width
        self.velocity.x = -1 * self.velocity.x
    end  
    if self.location.y > height - self.height then
        self.location.y = height - self.height
        self.velocity.y = -1 * self.velocity.y
    elseif self.location.y < self.height then
        self.location.y = self.height
        self.velocity.y = -1 * self.velocity.y
    end  
end

function Mover:applyForce(force)
    self.acceleration:add(force * self.mass)
end
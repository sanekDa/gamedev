require("vector")
require("mover")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    
    local location = Vector:create(width / 4 * 3, height / 2)
    local velocity = Vector:create(0,0)
    mover = Mover:create(location, velocity)

    local location = Vector:create(width / 4, height / 2)
    local velocity = Vector:create(0,0)
    bmover = Mover:create(location, velocity, 1.5)
    
    gravity = Vector:create(0, 0.01)
    isGravity = false
    floating = Vector:create(0, -0.02)
    isFloating = false
   
    wind = Vector:create(0.1, 0)
    isWind = false
end

function love.update(dt)
    if isGravity then
        mover:applyForce(gravity)
        bmover:applyForce(gravity)
    end
    if isWind then
        mover:applyForce(wind)
        bmover:applyForce(wind)
    end
    if isFloating then
        mover:applyForce(floating)
        bmover:applyForce(floating)
    end

    friction = (mover.velocity * -1):norm()
    bfriction = (bmover.velocity * -1):norm()
    if friction then
        if (mover.location.x < width/2) then
            friction:mul(0.02)
        else
            friction:mul(-0.02)
        end
        mover:applyForce(friction)
    end
    if bfriction then
        if (bmover.location.x < width/2) then
            bfriction:mul(0.002)
        else
            bfriction:mul(-0.002)
        end
        bmover:applyForce(bfriction)
    end
    

    mover:update()
    mover:check_boundaries()

    bmover:update()
    bmover:check_boundaries()
end

function love.draw()
    love.graphics.print(tostring(mover.velocity), mover.location.x + mover.size, mover.location.y + mover.size)
    love.graphics.print("g: " .. tostring(isGravity) .. " w: " .. tostring(isWind) .. " f: " .. tostring(isFloating), 10, height - 20 )
    
    love.graphics.print(tostring(bmover.velocity), bmover.location.x + bmover.size, bmover.location.y + bmover.size)
    love.graphics.print("g: " .. tostring(isGravity) .. " w: " .. tostring(isWind) .. " f: " .. tostring(isFloating), 10, height - 20 )

    love.graphics.setColor(200/255, 0, 0, 0.5)
    love.graphics.rectangle("fill", 0, 0, width/2, height)
    love.graphics.setColor(0, 0, 200/255, 0.5)
    love.graphics.rectangle("fill", width/2, 0, width, height)
    love.graphics.setColor(1, 1, 1)
    mover:draw()
    bmover:draw()
end

function love.keypressed(key)
    if (key == "g") then
        isGravity = not isGravity
    end
    if (key == "f") then
        isFloating = not isFloating
    end
    if (key == "w") then
        isWind = not isWind
        if isWind then
            wind.x = -1 * wind.x
        end
    end
end
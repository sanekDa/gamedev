Path = {}
Path.__index = Path

function Path:create(points)
    local path = {}
    setmetatable(path, Path)
    path.points = points
    path.d = 20 or d
    return path
end

function Path:draw()
    for i = 1, #self.points - 1 do
        start = self.points[i]
        stop = self.points[i + 1]
        local r, g, b, a = love.graphics.getColor()
        love.graphics.setLineWidth(self.d)
        love.graphics.setColor(0.31, 0.31, 0.31, 0.7)
        love.graphics.line(start.x, start.y, stop.x, stop.y)
        love.graphics.setBlendMode("replace")
        love.graphics.circle("fill", start.x, start.y, self.d / 2)
        love.graphics.circle("fill", stop.x, stop.y, self.d / 2)

        love.graphics.setColor(0., 0., 0., 0.7)
        love.graphics.setLineWidth(self.d / 10)
        love.graphics.line(start.x, start.y, stop.x, stop.y)

        love.graphics.setColor(r, g, b, a)
    end
end
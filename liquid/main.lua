require("vector")
require("mover")
require("moverRect")
require("liquid")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()    
    gravity = Vector:create(0, 0.1)
    water = Liquid:create(200, height - 300, 350, 300, 0.2)
    n = 4
    local velocity = Vector:create(0,0)
    rect1 = MoverRect:create(Vector:create(315,25), 20, 40, velocity, love.math.random(4, 15) / 200)
    rect2 = MoverRect:create(Vector:create(345,25), 40, 20, velocity, love.math.random(4, 15) / 200)
    movers = {}
    for i = 1, n do
        local location = Vector:create(i*150, height/4)
        local velocity = Vector:create(0,0)
        mover = Mover:create(location, velocity, love.math.random(4, 15) / 10)
        movers[i] = mover
    end
end

function love.update(dt)
    for i = 1, n do
        local mover = movers[i]
        mover:applyForce(gravity)
        rect1:applyForce(gravity)
        rect2:applyForce(gravity)
        friction = (mover.velocity * -1):norm()
        if friction then
            friction:mul(0.03)
            mover:applyForce(friction)
        end
        if water:isInside(mover) then
            local mag = mover.velocity:mag()
            local drag = water.c * mag * mag
            local dragF = (mover.velocity * -1):norm()
            if dragF then
                dragF:mul(drag)
                mover:applyForce(dragF)
            end
        end
        rect1:update()
        rect2:update()
        mover:update()
        mover:check_boundaries()
    end
end

function love.draw()
    rect1:draw()
    rect2:draw()
    water:draw()
    love.graphics.setColor(52/255, 235/255, 210/255)
    for i = 1, n do
        local mover = movers[i]
        mover:draw()
    end
end

function love.keypressed(key)
    if (key == "g") then
        isGravity = not isGravity
    end
end
MoverRect = {}
MoverRect.__index = MoverRect

function MoverRect:create(location, w, h, velocity, mass)
    local moverRect = {}
    setmetatable(moverRect, MoverRect)
    moverRect.location = location
    moverRect.velocity = velocity
    moverRect.acceleration = Vector:create(0, 0)
    moverRect.w = w
    moverRect.h = h
    moverRect.mass = mass or 1
    return moverRect
end

function MoverRect:random()
    local location = Vector:create()
    location.x = love.math.random(0, love.graphics.getWidth())
    location.y = love.math.random(0, love.graphics.getHeight())
    local velocity = Vector:create()
    velocity.x = love.math.random(-2, 2)
    velocity.y = love.math.random(-2, 2)
    return moverRect:create(location, velocity)
end

function MoverRect:draw()
    print("kek")
    love.graphics.rectangle("fill", self.location.x, self.location.y, self.w, self.h)
end

function MoverRect:update()
    self.velocity:add(self.acceleration)
    self.location:add(self.velocity)
    self.acceleration:mul(0)
end

function MoverRect:check_boundaries()
    if self.location.x > width - self.w then
        self.location.x = width - self.w
        self.velocity.x = -1 * self.velocity.x
    elseif self.location.x < self.w then
        self.location.x = self.w
        self.velocity.x = -1 * self.velocity.x
    end  
    if self.location.y > height - self.h then
        self.location.y = height - self.h
        self.velocity.y = -1 * self.velocity.y
    elseif self.location.y < self.h then
        self.location.y = self.h
        self.velocity.y = -1 * self.velocity.y
    end  
end

function MoverRect:applyForce(force)
    self.acceleration:add(force * self.mass)
end
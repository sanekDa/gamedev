Wave = {}
Wave.__index = Wave

function Wave:create(posx, posy, wave_width, A, angleVel, angle)
    local wave = {}
    setmetatable(wave, Wave)
    wave.posx = posx
    wave.posy = posy
    wave.wave_width = wave_width
    wave.A = A
    wave.angleVel = angleVel
    wave.angle = angle
    return wave
end

function Wave:draw()
    for x = self.posx, self.posx + self.wave_width, 1 do
        self.y = self.A * math.sin((self.angle + x / 240) * 4)
        self.y = self.y + self.A * math.sin((self.angle + x / 240) * 7)
        love.graphics.setColor(255, 255, 255)
        love.graphics.circle("line", x, self.y + self.posy / 2, 10)
        love.graphics.setColor(181, 179, 179, 0.5)
        love.graphics.circle("fill", x, self.y + self.posy / 2, 10)
    end
    self.angle = self.angle  + self.angleVel;
    -- love.graphics.circle("fill", width / 2 + x, height / 2 + y, 20)
    love.graphics.line(0, height / 2, width, height / 2)
    love.graphics.line(width / 2, 0, width / 2, height)
end

function Wave:update()

end

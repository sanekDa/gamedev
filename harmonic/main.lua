require("wave")

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()

    
    local A = 20 -- Амплитуда
    local angleVel = 0.01 -- Скорость изменения угла
    local angle = 0
    local posx = 50
    local posy = height / 4 * 6
    local wave_width = width/4
    wave = Wave:create(posx, posy, wave_width, A, angleVel, angle)

    local A = 40 -- Амплитуда
    local angleVel = 0.09 -- Скорость изменения угла
    local angle = 20
    local posx = 50
    local posy = 250
    local wave_width = width/4
    wave2 = Wave:create(posx, posy, wave_width, A, angleVel, angle)

    local A = 5 -- Амплитуда
    local angleVel = 0.01 -- Скорость изменения угла
    local angle = 0
    local posx = width / 2 + 50
    local posy = height / 4 * 6
    local wave_width = width/4
    wave3 = Wave:create(posx, posy, wave_width, A, angleVel, angle)

    local A = 80 -- Амплитуда
    local angleVel = 0.01 -- Скорость изменения угла
    local angle = 20
    local posx = width / 2 + 50
    local posy = 250
    local wave_width = width/4
    wave4 = Wave:create(posx, posy, wave_width, A, angleVel, angle)
end

function love.update()
    wave:update()
    wave2:update()
    wave3:update()
    wave4:update()
end

function love.draw(dt)
    wave:draw()
    wave2:draw()
    wave3:draw()
    wave4:draw()
end
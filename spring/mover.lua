Mover = {}
Mover.__index = Mover

function Mover:create(position, mass)
    local mover = {}
    setmetatable(mover, Mover)
    mover.position = position
    mover.velocity = Vector:create(0, 0)
    mover.acceleration = Vector:create(0, 0)
    mover.mass = mass or 10
    mover.damping = 0.955
    mover.dragging = false
    mover.dragOffset = Vector:create(0, 0)
    return mover
end

function Mover:draw()
    local tp = "fill"
    if self.dragging then
        tp = "line"
    end
    love.graphics.circle(tp, self.position.x, self.position.y, self.mass)
end

function Mover:update()
    self:drag()
    self.velocity:add(self.acceleration)
    self.velocity:mul(self.damping)
    self.position:add(self.velocity)
    self.acceleration:mul(0)
end

function Mover:clicked(mousex, mousey)
    local d = math.dist(mousex, mousey, self.position.x, self.position.y)
    if d < self.mass then
        self.dragging = true
        self.dragOffset.x = self.position.x - mousex
        self.dragOffset.y = self.position.y - mousey
    end
end

function Mover:stopDragging()
        self.dragging = false
end

function Mover:drag(mousex, mousey)
    if self.dragging then
        local x, y = love.mouse.getPosition()
        self.position.x = x + self.dragOffset.x
        self.position.y = y + self.dragOffset.y
    end
end

function Mover:applyForce(force)
    local f = force / self.mass
    self.acceleration:add(f)
end
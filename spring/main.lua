require("vector")
require("mover")
require("spring")

function math.dist(x1, y1, x2, y2)
    return ((x2 - x1)^2 + (y2 - y1)^2)^0.5
end

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()


    gravity = Vector:create(0, 2)
    
    mover = Mover:create(Vector:create(width / 2, height / 2), 40)
    mover2 = Mover:create(Vector:create(width / 2, height / 4 * 3), 40)

    spring = Spring:create(width / 2, 100, 200)
    spring2 = Spring:create(mover.position.x, mover.position.y, 200)
end

function love.update(dt)
    mover:applyForce(gravity)
    mover2:applyForce(gravity)
    spring:connect(mover)
    spring:constrainLength(mover, 100, 400)
    spring2:connect(mover2)
    spring2:constrainLength(mover2, 100, 400)
    mover:update()
    mover2:update()
end

function love.draw()
    spring:draw()
    spring:drawLine(mover)
    spring2:draw()
    spring2:drawLine(mover2)
    mover:draw()
    mover2:draw()
end

function love.keypressed(key)

end

function love.mousepressed(x, y, button, istouch)
    if button == 1 then
        mover:clicked(x, y)
    end
end

function love.mousereleased(x, y, button, istouch)
    if button == 1 then
        mover:stopDragging()
    end
end
Boid = {}
Boid.__index = Boid

function Boid:create(x, y)
    local boid = {}
    setmetatable(boid, Boid)

    boid.position = Vector:create(x,y)
    boid.velocity = Vector:create(math.random(-10,10) / 10, math.random(-10,10) / 10)

    boid.acceleration = Vector:create(0,0)

    boid.r = 5
    boid.verticles = {0, -boid.r*2, -boid.r, boid.r*2, boid.r, boid.r*2}

    boid.maxSpeed = 4
    boid.maxForce = 0.1

    boid.wtheta = 0

    boid.target = Vector:create(0,0)

    return boid
end

function Boid:seek(target)
    local desired = target - self.position
    desired:mag()
    desired:norm()
    desired:mul(self.maxSpeed)
    local steer = desired - self.velocity
    steer:limit(self.maxForce)
    return steer
end

function Boid:separate(boids)
    local sep = 25
    local steer  = Vector:create(0, 0)
    local count = 0
    for i = 0, #boids do
        local boid = boids[i]
        local d = self.position:distTo(boid.position)
        if d > 0 and d < sep then
            local diff = self.position - boid.position
            diff:norm()
            diff:div(d)
            steer:add(diff)
            count = count + 1
        end
    end

    if count > 0 then
        steer = steer / count
    end

    if steer:mag() > 0 then
        steer:norm()
        steer:mul(self.maxSpeed)
        steer:sub(self.velocity)
        steer:limit(self.maxForce)
    end

    return steer
end

function Boid:align(others)
    local ndist = 50
    local sum = Vector:create(0, 0)
    local count = 0

    for i = 1, #others do 
        local other = others[i]
        local d = self.position:distTo(other.position)
        if d > 0 and d < ndist then
            sum:add(other.velocity)
            count = count + 1 
        end
    end

    if count > 0 then
        sum:div(count)
        sum:norm()
        sum:mul(self.maxSpeed)
        local steer = sum - self.velocity
        steer:limit(self.maxForce)
        return steer
    end
    return Vector:create(0, 0)
end

function Boid:cohesion(others)
    local ndist = 50
    local sum = Vector:create(0, 0)
    local count = 0

    for i = 1, #others do 
        local other = others[i]
        local d = self.position:distTo(other.position)
        if d > 0 and d < ndist then
            sum:add(other.position)
            count = count + 1 
        end
    end

    if count > 0 then
        sum:div(count)
        return self:seek(sum)
    end
    return Vector:create(0, 0)
end

function Boid:flock(boids, target)
    -- self:applyForce(self:seek(target))
    local sep = self:separate(boids)
    local align = self:align(boids)
    local cohesion = self:cohesion(boids)

    sep:mul(2.5)
    align:mul(1)
   

    if isAlign then
        self:applyForce(align)
    end

    if isSep then
        self:applyForce(sep)
    end

    if isCohesion then
        self:applyForce(cohesion)
    end
end

function Boid:draw()
    local theta = self.velocity:heading() + math.pi/2
    love.graphics.push()
    love.graphics.translate(self.position.x,self.position.y)
    love.graphics.rotate(theta)
    love.graphics.polygon("fill",self.verticles)
    love.graphics.pop()
end

function Boid:borders()
    if self.position.x < -self.r then
        self.position.x = width + self.r
    end
    if self.position.y < -self.r then
        self.position.y = height + self.r
    end
    if self.position.x > width + self.r then
        self.position.x = -self.r
    end
    if self.position.y > height + self.r then
        self.position.y = -self.r
    end
end

function Boid:update(boids, target)
    self:flock(boids, target)
    self.velocity:add(self.acceleration)
    self.velocity:limit(self.maxSpeed)
    self.position:add(self.velocity)
    self.acceleration:mul(0)
    self:borders()
end

function Boid:applyForce(force)
    self.acceleration:add(force)
end
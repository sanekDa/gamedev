require("vector")
require("boid")

json = require "json"
socket = require "socket"
local address, port = "localhost", 12345
udp = socket.udp()

udp:setpeername(address, port)
udp:settimeout(0)

function love.load()
    width = love.graphics.getWidth()
    height = love.graphics.getHeight()
    id = "lolkek"
    ready = false
    que = -1

    env = nil
    info = nil
    last_vector = nil
end

function love:update(dt)
    data = udp:receive()
    -- print(que)
    if not (data) and que == -1 then
        msg = {cmd ="create", id = id, color = {0, 1, 1}}
        udp:send(json.encode(msg))
        ready = false
    end
    if data then
        ready = true
        data = json.decode(data)
        if data["error"] then 
            que = -1
            msg = {cmd ="create", id = id, color = {0, 1, 1}}
            udp:send(json.encode(msg))
            ready = false
        elseif que == -1 then
            que = 0
        elseif que == 1 then
            info = data
        elseif que == 2 then
            env = data
        end
    end
    if ready then
        print(json.encode(data))
        if que == 0 then
            msg = {cmd ="info", id = id}
            udp:send(json.encode(msg))
            que = 1
        elseif que == 1 then
            msg = {cmd ="env", id = id}
            udp:send(json.encode(msg))
            que = 2
        elseif que == 2 then
            my_coords = Vector:create(info["x"], info["y"])
            local food_target = -1
            local enem_target = -1
            if data["food"] and #data["food"] > 0 then
                food = data["food"]
                near_food = Vector:create(food[1]["x"], food[1]["y"])
                min_dist = my_coords:distTo(near_food)
                for i = 1, #food do
                    coords = Vector:create(food[i]["x"], food[i]["y"])
                    local dist = my_coords:distTo(coords)
                    if dist < min_dist then
                        min_dist = dist
                        near_food = coords
                    end
                end
                food_target = near_food - my_coords
                -- msg = {cmd ="force", id = id, x = target.x, y = target.y}
                -- udp:send(json.encode(msg))
            end
            if data["other"] and #data["other"] > 0 then
                mylife = info["life"]
                other = data["other"]
                near_enem = Vector:create(other[1]["x"], other[1]["y"])
                min_dist = my_coords:distTo(near_enem)
                bigger_enemy = -1
                for i = 1, #other do
                    coords = Vector:create(other[i]["x"], other[i]["y"])
                    local dist = my_coords:distTo(coords)
                    if dist < min_dist and mylife > other[i]["life"] + 15 then
                        min_dist = dist
                        near_enem = coords
                    end
                    if mylife < other[i]["life"] then
                        bigger_enemy = coords
                        bigger_enemy:mul(-1)
                    end
                end
                enem_target = near_enem - my_coords
                if not(bigger_enemy == -1) then
                    enem_target = bigger_enemy
                end
            end
            local result

            if not(enem_target == -1) then
                result = enem_target
            elseif not(food_target == -1) then
                result = food_target
            else
                if not(last_vector) then
                    last_vector = Vector:create(love.math.random(10,100),love.math.random(10,100))
                end
                self_size = 100
                if my_coords.x == nil then
                    love.load()
                end
                if my_coords.x + self_size > 800 * 4 or my_coords.y + self_size > 600 * 4 or my_coords.x - self_size < 0 or my_coords.y - self_size < 0 then
                    last_vector = Vector:create(800 * 2, 600 * 2) - my_coords
                end
                result = last_vector
            end
            if result then
                last_vector = result
                msg = {cmd ="force", id = id, x = result.x, y = result.y}
                udp:send(json.encode(msg))
            end
            que = 0
        end


        ready = false

    end
end

function love.draw()
    -- r, g, b, a = love.graphics.getColor()
    -- if isSep then
    --     love.graphics.setColor(1, 0, 0)
    -- else
    --     love.graphics.setColor(0, 0, 1)
    -- end
    -- love.graphics.circle("fill", 10, 10, 5)
    -- love.graphics.setColor(r, g, b, a)

end

function love.keypressed(key)
    if key == "space" then
        que = 2
    elseif key == "e" then
        msg = {cmd ="env", id = id}
        udp:send(json.encode(msg))
    elseif key == "down" then
        
    elseif key == "left" then
        msg = {cmd ="force", id = id, x = -2, y = 0}
        udp:send(json.encode(msg))
    elseif key == "i" then
        msg = {cmd ="info", id = id}
        udp:send(json.encode(msg))
    end
end